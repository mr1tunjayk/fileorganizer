package filehelper

import (
	"path/filepath"
	"os"
)

type filehelper struct {
	root string
}

func Newfilehelper(root string) *filehelper{
	f := new(filehelper)
	f.root = root

	return f
}

func (f *filehelper) GetAllFiles() ([]string, error) {
	filesList := [] string {}
	err := filepath.Walk(f.root, func(path string, f os.FileInfo, err error) error {
		filesList = append(filesList, path)
		return nil
	})

	if err != nil {
		return nil, err
	}
	return filesList, nil
}
