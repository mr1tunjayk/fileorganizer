package main

import (
	"fmt"
	"fileOrganizer/filehelper"
)

func main() {
	fHelper := filehelper.Newfilehelper("/Users/x4jk")
	filesList, err := fHelper.GetAllFiles()
	
	if err != nil {
		fmt.Println(err.Error())
	} else {
		for _, file := range filesList {
			fmt.Printf("File name [%s]\n", file)
		}
	}
}
